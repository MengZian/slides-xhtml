/*!
Slides.js
date 1687522550870 [Fri Jun 23 2023 20:15:50 GMT+0800 (China Standard Time)]
(c) 2023 Bright_Leader
Licensed under the Apache-2.0.

dependenc(y/ies):
- Bright.js date >= 1687485630671
- DOMOperation.js date >= 1687494211485
*/
(function exportFunction(globalObject,factory){"use strict";
/*
written in ECMAScript 5 specification, with ECMAScript 6 keywords "let" and "const"
- This script is meant to be ECMAScript 3-compliant.
- When all "let" and "const" keywords are replaced with keyword "var",
- the script is ECMAScript 3-compliant.

----

- Standard ECMA-262
http://www.ecma-international.org/publications-and-standards/standards/ecma-262/

- ECMA-262, 3rd edition, December 1999
http://www.ecma-international.org/wp-content/uploads/ECMA-262_3rd_edition_december_1999.pdf

- ECMA-262, 5th edition, December 2009
http://www.ecma-international.org/wp-content/uploads/ECMA-262_5th_edition_december_2009.pdf

- ECMA-262, 5.1th edition, June 2011
http://262.ecma-international.org/5.1/index.html
http://www.ecma-international.org/wp-content/uploads/ECMA-262_5.1_edition_june_2011.pdf

- ECMA-262, 6th edition, June 2015
http://262.ecma-international.org/6.0/index.html
http://www.ecma-international.org/wp-content/uploads/ECMA-262_6th_edition_june_2015.pdf
*/
  const typeFunction="function";

  const isPrimitive=function(value){
    const typeOfValue=typeof value;
    return !value||(typeOfValue!=="object"&&typeOfValue!==typeFunction);
  };

  const returnOrThrowObjectType=function(){

    function valueIsPrimitiveError(value){
      return(
        "\x22"+
        value+
        "\x22 (type:\x20"+
        (value!==null?typeof value:value)+
        ") is not an object type (an object or a function)!"
      );
    }

    return function(value){
      if(isPrimitive(value))throw valueIsPrimitiveError(value);
      else return value;
    };
  }();

  returnOrThrowObjectType(globalObject);

  const constructor_key="constructor",
    prototype_key="prototype";

  const Function_constructor=exportFunction[constructor_key],
    Object_constructor={}[constructor_key];

  const Function_prototype=Function_constructor[prototype_key],
    Object_prototype=Object_constructor[prototype_key];

  const call_key="call";

  const Function_call=Function_prototype[call_key],
    Object_hasOwnProperty=Object_prototype.hasOwnProperty;

  const relinquishFunction=function(item,key,target){
    returnOrThrowObjectType(target);
    let targetOriginallyHasOwnTargetProperty,
      targetOriginalTargetPropertyValue;
    targetOriginallyHasOwnTargetProperty=Function_call[call_key](Object_hasOwnProperty,target,key);
    targetOriginalTargetPropertyValue=target[key];
    return function(){
      if(target[key]===item){ /* relinquishes the target property only when its value is the current item */
        (
          !targetOriginallyHasOwnTargetProperty&&
          Function_call[call_key](Object_hasOwnProperty,target,key)
        )?(delete target[key]):(target[key]=targetOriginalTargetPropertyValue);
        /* re-evaluates these variables */
        targetOriginallyHasOwnTargetProperty=Function_call[call_key](Object_hasOwnProperty,target,key);
        (
          key in target&&
          target[key]!==item&& /* whether the property value is successfully changed */
          (targetOriginalTargetPropertyValue=target[key])
        );
      }
      return item;
    };
  };

  const tryES5CreateNullPrototypeObject=function(){

    const create=Object_constructor.create;
    const createIsFunction=typeof create===typeFunction;

    return function(){
      try{
        return returnOrThrowObjectType(
          createIsFunction&&
          create(null)
        );
      }catch(_){
        return new Object_constructor();
      }
    };
  }();

/* explicitly informs dependenc(y/ies) */
  const dep0="Bright",
    dep1="DOMOperation";

  const name="Slides";

  const relinquish_key="relinquish",
    useRequireJS_key="useRequireJS",
    isAsynchronous_key="isAsynchronous";

/* eslint {
     "no-undef": [
       "error",
       {
         "typeof": true
       }
     ]
   }
--
ESLint [ http://eslint.org/ ] configurations
*/

  let define_,
    isAsynchronous;
  const useRequireJS=(
    typeof define===typeFunction&& /* eslint-disable-line no-undef */
    typeof (define_=define)===typeFunction&& /* eslint-disable-line no-undef */
    typeof requirejs===typeFunction&& /* eslint-disable-line no-undef */
    !isPrimitive(define_.amd)
  ); /* RequireJS included? [ http://requirejs.org/ ] (based on version 2.0.4 [ http://requirejs.org/docs/release/2.0.4/comments/require.js ] ) */

  isAsynchronous=false;

  useRequireJS?(
    define_(
      [dep0,dep1],
      function(){
        const args=arguments;
        const dependencies=tryES5CreateNullPrototypeObject();
        let index;
        index=-1;
        dependencies[dep0]=args[index+=1];
        dependencies[dep1]=args[index+=1];
        const product=factory(dependencies,globalObject);
        product[relinquish_key]=relinquishFunction(product,name,globalObject);
        product[useRequireJS_key]=useRequireJS;
        product[isAsynchronous_key]=isAsynchronous;
        name in globalObject||(globalObject[name]=product);
        return product;
      }
    )
  ):(globalObject[name]=function(){
    const dependencies=tryES5CreateNullPrototypeObject();
    dependencies[dep0]=globalObject[dep0];
    dependencies[dep1]=globalObject[dep1];
    const product=factory(dependencies,globalObject);
    product[relinquish_key]=relinquishFunction(product,name,globalObject);
    product[useRequireJS_key]=useRequireJS;
    product[isAsynchronous_key]=isAsynchronous;
    return product;
  }());

  isAsynchronous=true;
  return;
})(
  this,
  function factoryFunction(dependencies,bwindow){"use strict";
/* @license
Copyright 2023 Bright_Leader

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
function isPrimitive(value){
  const typeOfValue=typeof value;
  return !value||(typeOfValue!=="object"&&typeOfValue!=="function");
}

const returnOrThrowObjectType=function(){

function valueIsPrimitiveError(value){
  return(
    "\x22"+
    value+
    "\x22 (type:\x20"+
    (value!==null?typeof value:value)+
    ") is not an object type (an object or a function)!"
  );
}

  return function(value){
    if(isPrimitive(value))throw valueIsPrimitiveError(value);
    else return value;
  };
}();

returnOrThrowObjectType(dependencies);

/* helper function(s) */

function isFunction(value){
  return typeof value==="function";
}

const returnOrThrowFunction=function(){

function notAFunctionError(value){
  return(
    "\x22"+
    value+
    "\x22 (type:\x20"+
    (value!==null?typeof value:value)+
    ") is not a function!"
  );
}

  return function(value){ /*
This functions comes in handy when checking foreign items from dependencies.
*/
    if(isFunction(value))return value;
    else throw notAFunctionError(value);
  };
}();

const Bright=returnOrThrowObjectType(dependencies.Bright),
  DOMOperation=returnOrThrowObjectType(dependencies.DOMOperation);

function isObjectType(value){
  const typeOfValue=typeof value;
  return !!value&&(typeOfValue==="object"||typeOfValue==="function");
}

/* native ECMAScript item(s) */

const True=true,
  False=false;

const constructor_key="constructor",
  prototype_key="prototype",
  call_key="call";

const Function_constructor=isPrimitive[constructor_key];

const Function_prototype=Function_constructor[prototype_key];

const Function_call=Function_prototype[call_key],
  Function_apply=Function_prototype.apply;

const Object_constructor={}[constructor_key],
  Array_constructor=[][constructor_key];

const Object_prototype=Object_constructor[prototype_key],
  Array_prototype=Array_constructor[prototype_key];

const tryES5DefineProperty=function(){

const defineProperty=Object_constructor.defineProperty;
const definePropertyIsFunction=isFunction(defineProperty);

  return function(target,propertyName,propertyValue,isWritable,isEnumerable,isConfigurable){
    returnOrThrowObjectType(target);
    definePropertyIsFunction?defineProperty(
      target,
      propertyName,
      {
        value:propertyValue,
        writable:isWritable,
        enumerable:isEnumerable,
        configurable:isConfigurable
      }
    ):(target[propertyName]=propertyValue);
    return target;
  };
}();

/* key(s) */

function getDefaultAllowedElementNames(){
  return [
    "html", /* per http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd */
    "head",
    "title",
    "base",
    "meta",
    "link",
    "style",
    "script",
    "noscript",
    "body",
    "div",
    "p",
    "h1",
    "h2",
    "h3",
    "h4",
    "h5",
    "h6",
    "ul",
    "ol",
    "li",
    "dl",
    "dt",
    "dd",
    "address",
    "hr",
    "pre",
    "blockquote",
    "ins",
    "del",
    "a",
    "span",
    "bdo",
    "br",
    "em",
    "strong",
    "dfn",
    "code",
    "samp",
    "kbd",
    "var",
    "cite",
    "abbr",
    "acronym",
    "q",
    "sub",
    "sup",
    "tt",
    "i",
    "b",
    "big",
    "small",
    "object",
    "param",
    "img",
    "map",
    "area",
    "form",
    "label",
    "input",
    "select",
    "optgroup",
    "option",
    "textarea",
    "fieldset",
    "legend",
    "button",
    "table",
    "caption",
    "thead",
    "tfoot",
    "tbody",
    "colgroup",
    "col",
    "tr",
    "th",
    "td"
  ];
}

const container_key="container",
  resources_key="resources",
  counter_key="counter",
  moreAllowedElementNames_key="moreAllowedElementNames";

const getApplicationKeys=function(){
  return [
    container_key,
    resources_key,
    counter_key,
    moreAllowedElementNames_key
  ];
};

/* dependenc(y/ies) */

const assignMultipleProperties=returnOrThrowFunction(DOMOperation.assignMultipleProperties),
  arrayLikeMap=returnOrThrowFunction(Bright.arrayLikeMap),
  isArrayLike=function(){

const isArrayLike_Bright=returnOrThrowFunction(Bright.isArrayLike);

    return function(value){
      return(
        isObjectType(value)&&
        Function_call[call_key](Function_apply,isArrayLike_Bright,this,arguments)
      );
    };
  }();

const length_key="length";

const allowedElementNames_key="allowedElementNames";

const Object_hasOwnProperty=Object_prototype.hasOwnProperty,
  Array_slice=Array_prototype.slice;

const Application=function(){

function constructorRequireNewError(value){
  return(
    "\x22"+
    value+
    "\x22 constructor: \x27new\x27 is required!"
  );
}

  return function constructorFn(configObject){
    const self=this;
    if(!(self instanceof constructorFn))throw constructorRequireNewError(constructorFn);
    returnOrThrowObjectType(configObject);
    let i_var,len,j0,j1;

    for(
      i_var=0,
      len=(j0=getApplicationKeys())[length_key];
      i_var<len;
      i_var+=1
    )(
      Function_call[call_key](
        Object_hasOwnProperty,
        configObject,
        j1=j0[i_var]
      )&&
      tryES5DefineProperty(
        self,

        j1,configObject[j1],

        True,True,True
      )
    );

    tryES5DefineProperty(
      self,

      i_var=allowedElementNames_key,
      (
        Function_call[call_key](Object_hasOwnProperty,configObject,i_var)&& /* uses custom value if there is one */
        isArrayLike(j0=configObject[i_var])
      )?Function_call[call_key](Array_slice,j0):(
        i_var in self&& /* gets it from (most likely) its prototype */
        isArrayLike(j0=self[i_var])
      )?Function_call[call_key](Array_slice,j0):getDefaultAllowedElementNames(), /* We want this to be unique on every instance. */

      True,False,True
    );

    return self;
  };
}();

const Application_prototype=Application[prototype_key];

assignMultipleProperties(
  Application_prototype,

  getApplicationKeys(),
  [
    null,
    null,
    0,
    null
  ],

  True,False,True
);

tryES5DefineProperty(
  Application_prototype,

  allowedElementNames_key,
  getDefaultAllowedElementNames(),

  True,False,True
);

const isUint32=returnOrThrowFunction(Bright.isUint32),
  arrayLikeIndexOf=returnOrThrowFunction(Bright.arrayLikeIndexOf);

const returnAllowedElementName=function(item,range){
  return Function_call[call_key](
    arrayLikeIndexOf,
    isArrayLike(range)?range:getDefaultAllowedElementNames(),
    item
  )>-1?item:"xml";
};

const isNode=returnOrThrowFunction(DOMOperation.isNode),
  createElement=returnOrThrowFunction(DOMOperation.createElement),
  smartSetDOMProperties=returnOrThrowFunction(DOMOperation.smartSetDOMProperties),
  attributesSetter=returnOrThrowFunction(DOMOperation.attributesSetter),
  smartAddClasses=returnOrThrowFunction(DOMOperation.smartAddClasses),
  changeElementStyle=returnOrThrowFunction(DOMOperation.changeElementStyle),
  smartAddEventListener=returnOrThrowFunction(DOMOperation.smartAddEventListener),
  createDocumentFragment=returnOrThrowFunction(DOMOperation.createDocumentFragment),
  appendChildren=returnOrThrowFunction(DOMOperation.appendChildren),
  createTextNode=returnOrThrowFunction(DOMOperation.createTextNode);

function isNotEmpty(value){
  return value!==null&&value!==void null;
}

const bwindowIsWindow=(
  isObjectType(bwindow)&&
  bwindow.window===bwindow&&
  bwindow.Function===Function_constructor&&
  bwindow.Object===Object_constructor
);

/* Document Object Model (DOM) Level 2 Core Specification [ http://www.w3.org/TR/2000/REC-DOM-Level-2-Core-20001113/ ] */

const bDocument=bwindowIsWindow&&bwindow.Document, /* http://www.w3.org/TR/2000/REC-DOM-Level-2-Core-20001113/core.html#i-Document */
  bdocument=bwindowIsWindow&&bwindow.document;

const bDocument_prototype=isObjectType(bDocument)&&bDocument[prototype_key];

const bDocument_createElementNS=isObjectType(bDocument_prototype)&&bDocument_prototype.createElementNS;

const String_constructor=""[constructor_key];

const String_prototype=String_constructor[prototype_key];

const String_indexOf=String_prototype.indexOf;

const defaultNameSpace_xml="http://www.w3.org/XML/1998/namespace";

const bDocument_createElementNS_isFunction=isFunction(bDocument_createElementNS);

function isEmpty(value){
  return value===void null||value===null;
}

function isStringPrimitive(value){
  return typeof value==="string";
}

const createElementNS=function(namespaceURI,qualifiedName){
  const args=arguments;
  let i_var,len,j0,
    newElement;
  if(bDocument_createElementNS_isFunction){
    i_var=namespaceURI; /* borrows this variable */
    switch(True){
      case isEmpty(qualifiedName):
      case isStringPrimitive(qualifiedName)&&!qualifiedName:
        break;
      case Function_call[call_key](String_indexOf,qualifiedName,":")>-1:
        i_var=(
          isStringPrimitive(namespaceURI)&&
          namespaceURI&&
          Function_call[call_key](String_indexOf,qualifiedName,"xml:")!==0
        )?namespaceURI:defaultNameSpace_xml;
    }
    newElement=Function_call[call_key](
      bDocument_createElementNS,
      bdocument,
      i_var, /* This argument must be treated carefully. */
      qualifiedName
    );
    for(i_var=2,len=args[length_key];i_var<len;i_var+=1)(
      isObjectType(j0=args[i_var])&&
      (i_var%2===0?smartSetDOMProperties:attributesSetter)(newElement,j0)
    );
    return newElement;
  }else return Function_call[call_key]( /* fallback solution */
    Function_apply,
    createElement,
    null,
    Function_call[call_key](Array_slice,args,1)
  );
};

const bElement=bwindowIsWindow&&bwindow.Element; /* http://www.w3.org/TR/2000/REC-DOM-Level-2-Core-20001113/core.html#ID-745549614 */

const bElement_prototype=isObjectType(bElement)&&bElement[prototype_key];

const bElement_setAttributeNS=isObjectType(bElement_prototype)&&bElement_prototype.setAttributeNS,
  bElement_setAttribute=isObjectType(bElement_prototype)&&bElement_prototype.setAttribute;

const isElement=returnOrThrowFunction(DOMOperation.isElement);

function isNotAnElementError(value){
  return(
    "\x22"+
    value+
    "\x22 (type:\x20"+
    (value!==null?typeof value:value)+
    ") is not a DOM element ( http://www.w3.org/TR/2000/REC-DOM-Level-2-Core-20001113/core.html#ID-745549614 )!"
  );
}

const defaultNameSpace_xmlns="http://www.w3.org/2000/xmlns/";

const bElement_setAttributeNS_isFunction=isFunction(bElement_setAttributeNS),
  bElement_setAttribute_isFunction=isFunction(bElement_setAttribute);

const setElementAttributeNS=function(targetElement,namespaceURI,qualifiedName,value){
  if(!isElement(targetElement))throw isNotAnElementError(targetElement);
  let doesntNeedFallback, /* This is for checking whether fallback solution is needed. */
    namespaceURI_repaired;
  if(doesntNeedFallback=bElement_setAttributeNS_isFunction){ /* eslint-disable-line no-cond-assign */
    namespaceURI_repaired=namespaceURI;
    switch(True){
      case isEmpty(qualifiedName):
      case isStringPrimitive(qualifiedName)&&!qualifiedName:
        break;
      case qualifiedName==="xmlns":
      case Function_call[call_key](String_indexOf,qualifiedName,"xmlns:")===0:
        namespaceURI_repaired=defaultNameSpace_xmlns;
        break;
      case Function_call[call_key](String_indexOf,qualifiedName,":")>-1:
        switch(True){
          case Function_call[call_key](String_indexOf,qualifiedName,"xml:")===0:
            namespaceURI_repaired=defaultNameSpace_xml;
            break;
          case (
            isStringPrimitive(namespaceURI)&&
            namespaceURI&&
            True /* The evaluation order must not be changed. */
          ):
            break;
          default: /* We can not handle this case. */
            doesntNeedFallback=False;
        }
    }
  }
  doesntNeedFallback?Function_call[call_key](
    bElement_setAttributeNS,targetElement,namespaceURI_repaired,qualifiedName,value
  ):(
    bElement_setAttribute_isFunction&&
    Function_call[call_key](bElement_setAttribute,targetElement,qualifiedName,value)
  );
  return targetElement;
};

const setElementAttributesNS=function(targetElement,configObject){
  let i_var,len,j0;
  if(isObjectType(configObject)){ /* This function provides 2 ways of batch setting element attributes with namespaces. */
    if(
      (i_var=length_key) in configObject&&
      isUint32(len=configObject[i_var])
    ){
      for(i_var=0;i_var<len;i_var+=1)(
        Function_call[call_key](Object_hasOwnProperty,configObject,i_var)&&
        isObjectType(j0=configObject[i_var])&&
        setElementAttributeNS(
          targetElement,
          j0.namespace,
          j0.name,
          j0.value
        )
      );
    }else for(i_var in configObject){
      if(Function_call[call_key](Object_hasOwnProperty,configObject,i_var)){
        if(isObjectType(len=configObject[i_var])){
          for(j0 in len)(
            Function_call[call_key](Object_hasOwnProperty,len,j0)&&
            setElementAttributeNS(targetElement,i_var,j0,len[j0]) /*
We always squeeze usage of variables
so that we do not have to declare more vairables than we actually need.
*/
          );
        }else bElement_setAttribute_isFunction&&Function_call[call_key](bElement_setAttribute,targetElement,i_var,len);
      }
    }
  }
  return targetElement;
};

const itemToElement=(function selfFn(item){
  if(isNode(item))return item;
  if(isPrimitive(item))return null;
  let newNode,tempKey,j0,
    tempDocumentFragment;
  const self=this;
  switch(item.type){
    case "element":
      j0=returnAllowedElementName(
        item.name,
        (
          isObjectType(self)&&
          (tempKey=allowedElementNames_key) in self
        )?self[tempKey]:null /* taking extensibility into consideration */
      );
      newNode=Function_call[call_key](
        Object_hasOwnProperty,
        item,
        tempKey="namespace"
      )?createElementNS(item[tempKey],j0):createElement(j0);
      (
        Function_call[call_key](
          Object_hasOwnProperty,
          item,
          tempKey="props"
        )&&
        isObjectType(j0=item[tempKey])&&
        smartSetDOMProperties(newNode,j0)
      );
      (
        Function_call[call_key](
          Object_hasOwnProperty,
          item,
          tempKey="attrs"
        )&&
        isObjectType(j0=item[tempKey])&&
        attributesSetter(newNode,j0)
      );
      (
        Function_call[call_key](
          Object_hasOwnProperty,
          item,
          tempKey="attrsNS"
        )&&
        isObjectType(j0=item[tempKey])&&
        setElementAttributesNS(newNode,j0)
      );
      (
        Function_call[call_key](
          Object_hasOwnProperty,
          item,
          tempKey="class"
        )&&
        isNotEmpty(j0=item[tempKey])&&
        smartAddClasses(newNode,j0)
      );
      (
        Function_call[call_key](
          Object_hasOwnProperty,
          item,
          tempKey="style"
        )&&
        isObjectType(j0=item[tempKey])&&
        changeElementStyle(newNode,j0)
      );
      (
        Function_call[call_key](
          Object_hasOwnProperty,
          item,
          tempKey="on"
        )&&
        isObjectType(j0=item[tempKey])&&
        smartAddEventListener(newNode,j0)
      );
      if(
        Function_call[call_key](
          Object_hasOwnProperty,
          item,
          tempKey="children"
        )&&
        isObjectType(j0=item[tempKey])
      ){
        tempDocumentFragment=createDocumentFragment();
        appendChildren(
          tempDocumentFragment,
          Function_call[call_key](arrayLikeMap,j0,selfFn,self)
        );
        appendChildren(newNode,tempDocumentFragment);
      }
      break;
    case (tempKey="text"): /* I swear this is coincidental. */
      newNode=createTextNode(
        (
          Function_call[call_key](Object_hasOwnProperty,item,tempKey)&&
          isNotEmpty(j0=item[tempKey])
        )?j0:""
      );
      break;
    case "document-fragment":
      newNode=createDocumentFragment();
      (
        Function_call[call_key](
          Object,hasOwnProperty,
          item,
          tempKey="children"
        )&&
        isObjectType(j0=item[tempKey])&&
        appendChildren(
          newNode,
          Function_call[call_key](arrayLikeMap,j0,selfFn,self)
        )
      );
  }
  return newNode;
});

const arrayLikeDeduplicate=returnOrThrowFunction(Bright.arrayLikeDeduplicate);

const Array_concat=Array_prototype.concat;

tryES5DefineProperty(
  Application_prototype,

  "configure",
  function(configObject){
    const self=returnOrThrowObjectType(this);
    if(isPrimitive(configObject))return self;
    let tempKey,tempValue,
      tempCondition0; /*
This function is nitpicky -
it modifies the "this" object only
when the source object has a desired value.
*/
    (
      Function_call[call_key](
        Object_hasOwnProperty,
        configObject,
        tempKey=container_key
      )&&
      isNode(tempValue=configObject[tempKey])&&
      tryES5DefineProperty(
        self,

        tempKey,tempValue,

        True,True,True
      )
    );
    (
      Function_call[call_key](
        Object_hasOwnProperty,
        configObject,
        tempKey=resources_key
      )&&
      isArrayLike(tempValue=configObject[tempKey])&&
      tryES5DefineProperty(
        self,

        tempKey,
        Function_call[call_key](Array_slice,tempValue),

        True,True,True
      )
    );
    (
      (tempCondition0=Function_call[call_key](
        Object_hasOwnProperty,
        configObject,
        tempKey=counter_key
      ))||
      (!isUint32(self[tempKey])) /* fixes this property if its value is erroneous */
    )&&tryES5DefineProperty(
      self,

      tempKey,
      tempCondition0&&isUint32(tempValue=configObject[tempKey])?tempValue:0,

      True,True,True
    );
    (
      Function_call[call_key](
        Object_hasOwnProperty,
        configObject,
        tempValue=moreAllowedElementNames_key
      )&&
      tryES5DefineProperty(
        self,

        tempKey=allowedElementNames_key,
        arrayLikeDeduplicate(Function_call[call_key](
          Array_concat,
          (
            tempKey in self&&
            isObjectType(tempCondition0=self[tempKey])
          )?Function_call[call_key](
            Array_slice,tempCondition0
          ):getDefaultAllowedElementNames(),
          isObjectType(tempValue=configObject[tempValue])?Function_call[call_key](Array_slice,tempValue):tempValue
        )),

        True,True,True
      )
    );
    return self;
  },

  True,False,True
);

const removeAllChildren=returnOrThrowFunction(DOMOperation.removeAllChildren),
  roundInteger=returnOrThrowFunction(Bright.roundInteger);

tryES5DefineProperty(
  Application_prototype,

  "navigate",
  function(num){
    const self=returnOrThrowObjectType(this);
    let i_var,j0,j1,
      tempDocumentFragment;
    (
      (i_var=container_key) in self&&
      (j1=isNode(j0=self[i_var]))&&
      removeAllChildren(j0) /* This function will clear all child nodes when possible. */
    );
    if(
      j1&&
      (i_var=resources_key) in self&&
      isArrayLike(j1=self[i_var])
    ){
      i_var=roundInteger(
        0,
        j1[length_key]-1,
        num
      );
      tryES5DefineProperty(
        self,

        counter_key,i_var,

        True,True,True
      );
      if(Function_call[call_key](Object_hasOwnProperty,j1,i_var)){
        tempDocumentFragment=createDocumentFragment();
        appendChildren(
          tempDocumentFragment,
          Function_call[call_key](itemToElement,self,j1[i_var])
        );
        appendChildren(j0,tempDocumentFragment);
      }
    }else{
      Function_call[call_key](
        Object_hasOwnProperty,
        self,
        i_var=counter_key
      )&&delete self[i_var];
      tryES5DefineProperty(
        self,

        i_var,0,

        True,True,True
      );
    }
    return self;
  },

  True,False,True
);

Application.getApplicationKeys=getApplicationKeys;
Application.getDefaultAllowedElementNames=getDefaultAllowedElementNames;
Application.returnAllowedElementName=returnAllowedElementName;
Application.createElementNS=createElementNS;
Application.isNotAnElementError=isNotAnElementError;
Application.setElementAttributeNS=setElementAttributeNS;
Application.setElementAttributesNS=setElementAttributesNS;
Application.itemToElement=itemToElement;

createElementNS.defaultNameSpace_xml=defaultNameSpace_xml;

setElementAttributeNS.defaultNameSpace_xml=defaultNameSpace_xml;
setElementAttributeNS.defaultNameSpace_xmlns=defaultNameSpace_xmlns;

(function(){

function sharedKeys(){
  return [
    "author",
    "date"
  ];
}

function sharedValues(){
  return [
    "Bright_Leader",
    1687522550870
  ];
}

assignMultipleProperties(Application,sharedKeys(),sharedValues(),True,False,True);
assignMultipleProperties(Application_prototype,sharedKeys(),sharedValues(),True,False,True);

  return;
})();

    return Application;
  }
);
(function(globalObject,execute){"use strict";

  const typeFunction="function";

  const isPrimitive=function(value){
    const typeOfValue=typeof value;
    return !value||(typeOfValue!=="object"&&typeOfValue!==typeFunction);
  };

  const returnOrThrowObjectType=function(){

    function valueIsPrimitiveError(value){
      return(
        "\x22"+
        value+
        "\x22 (type:\x20"+
        (value!==null?typeof value:value)+
        ") is not an object type (an object or a function)!"
      );
    }

    return function(value){
      if(isPrimitive(value))throw valueIsPrimitiveError(value);
      else return value;
    };
  }();

  returnOrThrowObjectType(globalObject);

  const Object_constructor={}.constructor;

  const tryES5CreateNullPrototypeObject=function(){

    const create=Object_constructor.create;
    const createIsFunction=typeof create===typeFunction;

    return function(){
      try{
        return returnOrThrowObjectType(
          createIsFunction&&
          create(null)
        );
      }catch(_){
        return new Object_constructor();
      }
    };
  }();

  const dep0="Bright",
    dep1="DOMOperation",
    dep2="Slides";

  let define_,
    requirejs_;
  const useRequireJS=(
    typeof define===typeFunction&&
    typeof (define_=define)===typeFunction&& /* eslint-disable-line no-undef */
    typeof requirejs===typeFunction&&
    typeof (requirejs_=requirejs)===typeFunction&& /* eslint-disable-line no-undef */
    !isPrimitive(define_.amd)
  ); /* RequireJS included? [ http://requirejs.org/ ] (based on version 2.0.4 [ http://requirejs.org/docs/release/2.0.4/comments/require.js ] ) */

  useRequireJS?(
    requirejs_(
      [dep0,dep1,dep2],
      function(){
        const args=arguments;
        const dependencies=tryES5CreateNullPrototypeObject();
        let index;
        index=-1;
        dependencies[dep0]=args[index+=1];
        dependencies[dep1]=args[index+=1];
        dependencies[dep2]=args[index+=1];
        dependencies.define=define_;
        dependencies.requirejs=requirejs_;
        return execute(globalObject,dependencies,useRequireJS);
      }
    )
  ):function(){
    const dependencies=tryES5CreateNullPrototypeObject();
    dependencies[dep0]=globalObject[dep0];
    dependencies[dep1]=globalObject[dep1];
    dependencies[dep2]=globalObject[dep2];
    execute(globalObject,dependencies,useRequireJS);
    return;
  }();
  return;
})(
  this,
  function(bwindow,dependencies,useRequireJS){"use strict";
function isPrimitive(value){
  const typeOfValue=typeof value;
  return !value||(typeOfValue!=="object"&&typeOfValue!=="function");
}

const returnOrThrowObjectType=function(){

function valueIsPrimitiveError(value){
  return(
    "\x22"+
    value+
    "\x22 (type:\x20"+
    (value!==null?typeof value:value)+
    ") is not an object type (an object or a function)!"
  );
}

  return function(value){
    if(isPrimitive(value))throw valueIsPrimitiveError(value);
    else return value;
  };
}();

returnOrThrowObjectType(bwindow);
returnOrThrowObjectType(dependencies);

function isFunction(value){
  return typeof value==="function";
}

const returnOrThrowFunction=function(){

function notAFunctionError(value){
  return(
    "\x22"+
    value+
    "\x22 (type:\x20"+
    (value!==null?typeof value:value)+
    ") is not a function!"
  );
}

  return function(value){
    if(isFunction(value))return value;
    else throw notAFunctionError(value);
  };
}();

const Bright=returnOrThrowObjectType(dependencies.Bright),
  DOMOperation=returnOrThrowObjectType(dependencies.DOMOperation),
  Slides=returnOrThrowFunction(dependencies.Slides);

const False=false,
  True=true;

const constructor_key="constructor",
  prototype_key="prototype",
  call_key="call";

const Function_constructor=isPrimitive[constructor_key];

const Function_prototype=Function_constructor[prototype_key];

const Function_call=Function_prototype[call_key];

const Object_constructor={}[constructor_key],
  Array_constructor=[][constructor_key];

const createElement=returnOrThrowFunction(DOMOperation.createElement),
  smartSetDOMProperties=returnOrThrowFunction(DOMOperation.smartSetDOMProperties),
  smartAddClasses=returnOrThrowFunction(DOMOperation.smartAddClasses),
  createDocumentFragment=returnOrThrowFunction(DOMOperation.createDocumentFragment),
  appendChildren=returnOrThrowFunction(DOMOperation.appendChildren),
  smartAddEventListener=returnOrThrowFunction(DOMOperation.smartAddEventListener),
  createTextNode=returnOrThrowFunction(DOMOperation.createTextNode),
  attributesSetter=returnOrThrowFunction(DOMOperation.attributesSetter),
  removeAllChildren=returnOrThrowFunction(DOMOperation.removeAllChildren),
  arrayLikeMap=returnOrThrowFunction(Bright.arrayLikeMap);

const arrayWithOneItem=function(item){
  const newArray=new Array_constructor(1);
  newArray[0]=item;
  return newArray;
};

const debugSwitch=True;

const arrowSVG=function(){ /* Scalable Vector Graphics (SVG) 1.1 (Second Edition) [ http://www.w3.org/TR/2011/REC-SVG11-20110816/ ] */

const createElementNS=returnOrThrowFunction(Slides.createElementNS);

const svg_namespace="http://www.w3.org/2000/svg";

  return function(rightDirection){
    const newElement=createElementNS(svg_namespace,"svg");
    attributesSetter(newElement,"version","1.1");
    attributesSetter(newElement,"viewBox","0 0 4.4 4.4");
    const tempDocumentFragment=createDocumentFragment();

    const tempElement=createElementNS(svg_namespace,"path");
    attributesSetter(tempElement,"d","m 0 1 h 2 v -1 l 2 2 l -2 2 v -1 h -2 z");
    attributesSetter(tempElement,"fill","#FFFFFF");
    attributesSetter(tempElement,"stroke","#000000");
    attributesSetter(tempElement,"stroke-width","0.1");
    attributesSetter(
      tempElement,
      "transform",
      (
        "matrix("+
        (rightDirection?"":"-")+
        "1 0 0 1\x20"+
        (rightDirection?0:4)+
        ".2 0.2)"
      )
    );
    appendChildren(tempDocumentFragment,tempElement);

    appendChildren(newElement,tempDocumentFragment);
    return newElement;
  };
}();

function isNotEmpty(value){
  return value!==null&&value!==void null;
}

function isEmpty(value){
  return value===void null||value===null;
}

const spanWithTextAndLang=function(text,lang){
  const newElement=createElement("span");
  isNotEmpty(lang)&&smartSetDOMProperties(newElement,"lang",lang);
  if(isEmpty(text))return newElement;
  const tempDocumentFragment=createDocumentFragment();
  appendChildren(
    tempDocumentFragment,
    createTextNode(text)
  );
  appendChildren(newElement,tempDocumentFragment);
  return newElement;
};

const createInputNumber=function(){
  const newElement=createElement("input");
  smartSetDOMProperties(newElement,"type","number");
  return newElement;
};

const createInputText=function(){
  const newElement=createElement("input");
  smartSetDOMProperties(newElement,"type","text");
  return newElement;
};

const createReadonlyInputText=function(){
  const newElement=createInputText();
  attributesSetter(newElement,"readonly","readonly");
  return newElement;
};

const createInputButtonWithValue=function(value){
  const newElement=createElement("input");
  smartSetDOMProperties(newElement,"type","button");
  isNotEmpty(value)&&smartSetDOMProperties(newElement,"value",value);
  return newElement;
};

const createStylesheet=function(path){
  const newElement=createElement("link");
  smartSetDOMProperties(newElement,"rel","stylesheet");
  smartSetDOMProperties(
    newElement,
    "href",
    path+".css"
  );
  if(debugSwitch){
    try{
      smartAddEventListener(newElement,"load",bwindow.console.log);
      smartAddEventListener(newElement,"error",bwindow.console.error);
    }catch(_){ /* */ }
  }
  return newElement;
};

const createJavaScriptElement=function(path){
  const newElement=createElement("script");
  smartSetDOMProperties(newElement,"type","text/javascript");
  smartSetDOMProperties(
    newElement,
    "src",
    path+".js"
  );
  if(debugSwitch){
    try{
      smartAddEventListener(newElement,"load",bwindow.console.log);
      smartAddEventListener(newElement,"error",bwindow.console.error);
    }catch(_){ /* */ }
  }
  return newElement;
};

const nDate=bwindow.Date;

function isObjectType(value){
  const typeOfValue=typeof value;
  return !!value&&(typeOfValue==="object"||typeOfValue==="function");
}

const factory=function(){

const setPage_key="setPage";

const onsetPage_key="on"+setPage_key;

const counter_key="counter",
  resources_key="resources",
  navigate_key="navigate";

const click_eventKey="click",
  change_eventKey="change";

const aboutDate=new nDate(1687526393826);

const createTextNodeDescription=function(text){
  const newObject=new Object_constructor();
  newObject.type="text";
  isNotEmpty(text)&&(newObject.text=text);
  return newObject;
};

const createPWithTextDescription=function(text){
  const newObject=new Object_constructor();
  newObject.type="element";
  newObject.name="p";
  isNotEmpty(text)&&(newObject.children=arrayWithOneItem(
    createTextNodeDescription(text)
  ));
  return newObject;
};

const createAnchorWithTextDescription=function(href,text){
  const newObject=new Object_constructor();
  newObject.type="element";
  newObject.name="a";
  isNotEmpty(href)&&(newObject.props={
    href:href
  });
  isNotEmpty(text)&&(newObject.children=arrayWithOneItem(
    createTextNodeDescription(text)
  ));
  return newObject;
};

const aboutResource=function(target){
  returnOrThrowObjectType(target).type="document-fragment";
  target.children=[
    createPWithTextDescription("Slides in XHTML"),
    createPWithTextDescription("Author: Bright_Leader"),
    {
      type:"element",
      name:"p",
      children:[
        createTextNodeDescription("Licensed under\x20"),
        createAnchorWithTextDescription("http://www.apache.org/licenses/LICENSE-2.0","Apache-2.0"),
        createTextNodeDescription(".")
      ]
    },
    {
      type:"element",
      name:"p",
      children:[
        createTextNodeDescription("Project page on\x20"),
        createAnchorWithTextDescription("http://gitee.com/MengZian/slides-xhtml","Gitee"),
        createTextNodeDescription("\x20and\x20"),
        createAnchorWithTextDescription("http://gitcode.net/MengZian/slides-xhtml","GitCode")
      ]
    },
    createPWithTextDescription(
      "Software date:\x20"+
      aboutDate+
      "\x20("+
      (aboutDate-0)+
      ")"
    )
  ];
  return target;
};

const scriptDate=new nDate();

function navigatorItem_classes(){
  return [
    "padding-horizontal-default",
    "navigator-item"
  ];
}

function pageIndicator_classes(){
  return [
    "flex-column",
    "page-indicator"
  ];
}

function hideOnPortrait_classes(){
  return [
    "hide-on-portrait",
    "dynamic-display"
  ];
}

function inputElement_classes(){
  return [
    "input-style0",
    "input-style1"
  ];
}

  return function(index){

    const newElement=createElement("div");

    smartSetDOMProperties(
      newElement,
      "id",
      "slides-app"+index
    );
    smartAddClasses(
      newElement,
      "container","padding-horizontal-default","padding-vertical-default",
      "slides-app",
      "about-date-"+(aboutDate-0),
      "script-date-"+(scriptDate-0),
      "factory-date-"+(new nDate()-0)
    );

const tempDocumentFragment=createDocumentFragment();

const containerElement=createElement("div");

const localApplication=new Slides({
  container:containerElement
});

smartAddClasses(containerElement,"resources-container");

const localEvents=new Object_constructor();
localEvents[onsetPage_key]=null;

const toPageEvent=function(num){
  Function_call[call_key](
    returnOrThrowFunction(localApplication[navigate_key]),
    localApplication,
    num
  );
  let tempFunction;
  (
    isFunction(tempFunction=localEvents[onsetPage_key])&&
    tempFunction(localApplication)
  );
  if(debugSwitch){
    try{
      bwindow.console.log(localApplication);
    }catch(_){ /* */ }
  }
  return;
};

const previousPageEvent=function(){
  return toPageEvent(localApplication[counter_key]-1);
};

const nextPageEvent=function(){
  return toPageEvent(localApplication[counter_key]+1);
};

const navigateButton=function(rightDirection){
  const newElement=createElement("button");
  smartAddClasses(
    newElement,
    "arrow-"+(rightDirection?"righ":"lef")+"t",
    "arrow",
    "image-container",
    "button-style1"
  );
  smartAddEventListener(
    newElement,
    click_eventKey,
    rightDirection?nextPageEvent:previousPageEvent
  );
  const tempDocumentFragment=createDocumentFragment();
  appendChildren(
    tempDocumentFragment,
    arrowSVG(rightDirection)
  );
  appendChildren(newElement,tempDocumentFragment);
  return newElement;
};

const switchPageDiv=function(rightDirection,hideOnPortrait){
  const newElement=createElement("div");
  smartAddClasses(
    newElement,
    navigatorItem_classes()
  );
  hideOnPortrait&&smartAddClasses(
    newElement,
    hideOnPortrait_classes()
  );
  const tempDocumentFragment=createDocumentFragment();
  appendChildren(
    tempDocumentFragment,
    navigateButton(rightDirection)
  );
  appendChildren(newElement,tempDocumentFragment);
  return newElement;
};

const navigateToPageInputNumber=function(){
  const newElement=createInputNumber();
  smartAddClasses(
    newElement,
    inputElement_classes()
  );
  smartSetDOMProperties(newElement,"value",1);
  smartAddEventListener(
    newElement,
    change_eventKey,
    function(){
      const self=this;
      if(isPrimitive(self))return;
      toPageEvent(self.value-1);
      return;
    }
  );
  smartAddEventListener(
    localEvents,
    setPage_key,
    function(app){
      smartSetDOMProperties(
        newElement,
        "value",
        isObjectType(app)?app[counter_key]+1:0
      );
      return;
    }
  );
  return newElement;
};

const pageNavigationBarDiv=function(hideOnPortrait){
  const newElement=createElement("div");
  smartAddClasses(
    newElement,
    navigatorItem_classes()
  );
  smartAddClasses(
    newElement,
    pageIndicator_classes()
  );
  hideOnPortrait&&smartAddClasses(
    newElement,
    hideOnPortrait_classes()
  );
  const tempDocumentFragment=createDocumentFragment();
  appendChildren(
    tempDocumentFragment,
    spanWithTextAndLang("To page number:\x20","en"),
    spanWithTextAndLang("跳转至页数：","zh"),
    createElement("br"),
    navigateToPageInputNumber()
  );
  appendChildren(newElement,tempDocumentFragment);
  return newElement;
};

const totalPageNumberInputText=function(){
  const newElement=createReadonlyInputText();
  smartAddClasses(
    newElement,
    inputElement_classes()
  );
  smartAddEventListener(
    localEvents,
    setPage_key,
    function(app){
      let tempValue;
      smartSetDOMProperties(
        newElement,
        "value",
        (
          isObjectType(app)&&
          isObjectType(tempValue=app[resources_key])
        )?tempValue.length:"?"
      );
      return;
    }
  );
  return newElement;
};

const totalPageNumberIndicatorBarDiv=function(hideOnPortrait){
  const newElement=createElement("div");
  smartAddClasses(
    newElement,
    navigatorItem_classes()
  );
  smartAddClasses(
    newElement,
    pageIndicator_classes()
  );
  hideOnPortrait&&smartAddClasses(
    newElement,
    hideOnPortrait_classes()
  );
  const tempDocumentFragment=createDocumentFragment();
  appendChildren(
    tempDocumentFragment,
    spanWithTextAndLang("Total page number(s):\x20","en"),
    spanWithTextAndLang("总页数：","zh"),
    createElement("br"),
    totalPageNumberInputText()
  );
  appendChildren(newElement,tempDocumentFragment);
  return newElement;
};

const indicatorsDiv=function(isTop){
  const newElement=createElement("div");
  smartAddClasses(
    newElement,
    "children-display-inline-block",
    "text-align-center",
    "align-center",
    "flex-container",
    "flex-row",
    "indicators",
    "indicators-"+(isTop?"top":"bottom")
  );
  const tempDocumentFragment=createDocumentFragment();
  appendChildren(
    tempDocumentFragment,
    switchPageDiv(False,False),
    switchPageDiv(True,True),
    pageNavigationBarDiv(!isTop),
    totalPageNumberIndicatorBarDiv(isTop),
    pageNavigationBarDiv(True),
    switchPageDiv(False,True),
    switchPageDiv(True,False)
  );
  appendChildren(newElement,tempDocumentFragment);
  return newElement;
};

const quickNavigationInputButton_arrayLikeMap=function(_,index0){
  const newElement=createInputButtonWithValue(index0+1);
  smartAddClasses(
    newElement,
    "input-style0",
    "input-style1"
  );
  smartAddEventListener(
    newElement,
    click_eventKey,
    function(){
      toPageEvent(index0);
      return;
    }
  );
  return newElement;
};

const quickNavigationDiv=function(isTop){
  const newElement=createElement("div");
  smartAddClasses(
    newElement,
    "padding-vertical-default",
    "text-align-center",
    "quick-navigation",
    "quick-navigation-"+(isTop?"top":"bottom")
  );
  isTop&&smartAddClasses(
    newElement,
    hideOnPortrait_classes()
  );
  smartAddEventListener(
    localEvents,
    setPage_key,
    function(app){
      removeAllChildren(newElement);
      let tempValue,
        tempDocumentFragment;
      if(
        isObjectType(app)&&
        isObjectType(tempValue=app[resources_key])
      ){
        tempDocumentFragment=createDocumentFragment();
        appendChildren(
          tempDocumentFragment,
          Function_call[call_key](
            arrayLikeMap,
            tempValue,
            quickNavigationInputButton_arrayLikeMap
          )
        );
        appendChildren(newElement,tempDocumentFragment);
      }
      return;
    }
  );
  return newElement;
};

appendChildren(
  tempDocumentFragment,
  indicatorsDiv(True),
  quickNavigationDiv(True),
  containerElement,
  quickNavigationDiv(False),
  indicatorsDiv(False)
);

appendChildren(
  tempDocumentFragment,
  function(){
    const newDocumentFragment=createDocumentFragment();
    const containerElement=createElement("div");
    const aboutApp=new Slides({
      container:containerElement,
      resources:[
        null,
        aboutResource(new Object_constructor())
      ]
    });
    const navigateToPage_aboutApp=function(element,pageNum){
      smartAddClasses(element,"input-style2");
      smartAddEventListener(
        element,
        click_eventKey,
        function(){
          Function_call[call_key](returnOrThrowFunction(aboutApp[navigate_key]),aboutApp,pageNum);
          return;
        }
      );
      return element;
    };
    appendChildren(
      newDocumentFragment,
      function(){
        const newElement=createElement("div");
        smartAddClasses(newElement,"padding-vertical-default");
        let pageNumIndex;
        pageNumIndex=-1;
        const tempDocumentFragment=createDocumentFragment();
        appendChildren(
          tempDocumentFragment,
          navigateToPage_aboutApp(
            createInputButtonWithValue("Collapse"),
            pageNumIndex+=1
          ),
          navigateToPage_aboutApp(
            createInputButtonWithValue("About this software"),
            pageNumIndex+=1
          )
        );
        appendChildren(newElement,tempDocumentFragment);
        return newElement;
      }(),
      containerElement
    );
    if(debugSwitch){
      try{
        bwindow.console.log(aboutApp);
      }catch(_){ /* */ }
    }
    return newDocumentFragment;
  }()
);

appendChildren( /* loads user stylesheet */
  tempDocumentFragment,
  createStylesheet("./user/user"),
  createStylesheet("./user/user.css3")
);

appendChildren( /* loads user script */
  tempDocumentFragment,
  function(){

const newElement=createJavaScriptElement("./user/slides-config");
smartAddEventListener(
  newElement,
  "load",
  function(){

const temp=bwindow.config;

Function_call[call_key](
  returnOrThrowFunction(localApplication.configure),
  localApplication,
  temp
);
toPageEvent(localApplication[counter_key]);
isObjectType(temp)&&(temp.app=localApplication);

    return;
  }
);

    return newElement;
  }()
);

appendChildren(newElement,tempDocumentFragment);

    return newElement;
  };
}();

const arrayLikeEvery=returnOrThrowFunction(Bright.arrayLikeEvery),
  getElementsByClassName=returnOrThrowFunction(DOMOperation.getElementsByClassName),
  replaceNode=returnOrThrowFunction(DOMOperation.replaceNode);

const bdocument=returnOrThrowObjectType(bwindow.document);

const callbackFn=function(){
  Function_call[call_key](
    arrayLikeEvery,
    getElementsByClassName("slides-area",bdocument),
    function(elem,index){
      replaceNode(
        factory(index),
        elem
      );
      return elem;
    }
  );
  return;
};

useRequireJS?(
  returnOrThrowFunction(bwindow.setTimeout)(callbackFn)
):Function_call[call_key](
  returnOrThrowFunction(bdocument.addEventListener),
  bdocument,
  "DOMContentLoaded",
  callbackFn
);

debugSwitch&&function(){

const tempDate=new nDate();

const bconsole=bwindow.console;

try{

  bconsole.log(bwindow);
  bconsole.log(dependencies);
  bconsole.info(Bright);
  bconsole.info(DOMOperation);
  bconsole.info(Slides);
  bconsole.log(factory);
  bconsole.log(callbackFn);

  bconsole.log(tempDate);
  bconsole.log(tempDate+"");
  bconsole.log(tempDate-0);

}catch(_){ /* */ }

  return;
}();

    return bwindow;
  }
);
/*
RequireJS [ http://requirejs.org/ ]
version 2.0.4 [ http://requirejs.org/docs/release/2.0.4/comments/require.js ]
*/
(function(){"use strict";

const typeFunction="function";

let requirejs_,
  requirejs_config;

(
  typeof requirejs===typeFunction&&
  typeof (requirejs_=requirejs)===typeFunction&&
  typeof (requirejs_config=requirejs_.config)===typeFunction&&
  requirejs_config({
    baseUrl:"./static/js"
  })
);

  return;
})();
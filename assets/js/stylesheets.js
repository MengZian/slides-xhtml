(function(globalObject,execute){"use strict";

  const typeFunction="function";

  const isPrimitive=function(value){
    const typeOfValue=typeof value;
    return !value||(typeOfValue!=="object"&&typeOfValue!==typeFunction);
  };

  const returnOrThrowObjectType=function(){

    function valueIsPrimitiveError(value){
      return(
        "\x22"+
        value+
        "\x22 (type:\x20"+
        (value!==null?typeof value:value)+
        ") is not an object type (an object or a function)!"
      );
    }

    return function(value){
      if(isPrimitive(value))throw valueIsPrimitiveError(value);
      else return value;
    };
  }();

  returnOrThrowObjectType(globalObject);

  const Object_constructor={}.constructor;

  const tryES5CreateNullPrototypeObject=function(){

    const create=Object_constructor.create;
    const createIsFunction=typeof create===typeFunction;

    return function(){
      try{
        return returnOrThrowObjectType(
          createIsFunction&&
          create(null)
        );
      }catch(_){
        return new Object_constructor();
      }
    };
  }();

  const dep0="Bright",
    dep1="DOMOperation";

  let define_,
    requirejs_;
  const useRequireJS=(
    typeof define===typeFunction&&
    typeof (define_=define)===typeFunction&& /* eslint-disable-line no-undef */
    typeof requirejs===typeFunction&&
    typeof (requirejs_=requirejs)===typeFunction&& /* eslint-disable-line no-undef */
    !isPrimitive(define_.amd)
  ); /* RequireJS included? [ http://requirejs.org/ ] (based on version 2.0.4 [ http://requirejs.org/docs/release/2.0.4/comments/require.js ] ) */

  useRequireJS?(
    requirejs_(
      [dep0,dep1],
      function(){
        const args=arguments;
        const dependencies=tryES5CreateNullPrototypeObject();
        let index;
        index=-1;
        dependencies[dep0]=args[index+=1];
        dependencies[dep1]=args[index+=1];
        dependencies.define=define_;
        dependencies.requirejs=requirejs_;
        return execute(globalObject,dependencies,useRequireJS);
      }
    )
  ):function(){
    const dependencies=tryES5CreateNullPrototypeObject();
    dependencies[dep0]=globalObject[dep0];
    dependencies[dep1]=globalObject[dep1];
    execute(globalObject,dependencies,useRequireJS);
    return;
  }();
  return;
})(
  this,
  function(bwindow,dependencies,useRequireJS){"use strict";
function isPrimitive(value){
  const typeOfValue=typeof value;
  return !value||(typeOfValue!=="object"&&typeOfValue!=="function");
}

const returnOrThrowObjectType=function(){

function valueIsPrimitiveError(value){
  return(
    "\x22"+
    value+
    "\x22 (type:\x20"+
    (value!==null?typeof value:value)+
    ") is not an object type (an object or a function)!"
  );
}

  return function(value){
    if(isPrimitive(value))throw valueIsPrimitiveError(value);
    else return value;
  };
}();

returnOrThrowObjectType(bwindow);
returnOrThrowObjectType(dependencies);

function isFunction(value){
  return typeof value==="function";
}

const returnOrThrowFunction=function(){

function notAFunctionError(value){
  return(
    "\x22"+
    value+
    "\x22 (type:\x20"+
    (value!==null?typeof value:value)+
    ") is not a function!"
  );
}

  return function(value){
    if(isFunction(value))return value;
    else throw notAFunctionError(value);
  };
}();

const Bright=returnOrThrowObjectType(dependencies.Bright),
  DOMOperation=returnOrThrowObjectType(dependencies.DOMOperation);

const constructor_key="constructor",
  prototype_key="prototype",
  call_key="call";

const Function_constructor=isPrimitive[constructor_key];

const Function_prototype=Function_constructor[prototype_key];

const Function_call=Function_prototype[call_key];

const createElement=returnOrThrowFunction(DOMOperation.createElement),
  smartSetDOMProperties=returnOrThrowFunction(DOMOperation.smartSetDOMProperties),
  appendChildren=returnOrThrowFunction(DOMOperation.appendChildren),
  createDocumentFragment=returnOrThrowFunction(DOMOperation.createDocumentFragment),
  arrayLikeMap=returnOrThrowFunction(Bright.arrayLikeMap);

const createStylesheet=function(path){
  const newElement=createElement("link");
  smartSetDOMProperties(newElement,"rel","stylesheet");
  smartSetDOMProperties(
    newElement,
    "href",
    path+".css"
  );
  return newElement;
};

const bdocument=returnOrThrowObjectType(bwindow.document);

const basePath="./static/css/";

const callbackFn=function(){

  const tempDocumentFragment=createDocumentFragment();
  appendChildren(
    tempDocumentFragment,
    Function_call[call_key](
      arrayLikeMap,
      [
        basePath+"pages",
        basePath+"pages.css3"
      ],
      createStylesheet
    )
  );
  appendChildren(bdocument.head,tempDocumentFragment);

  return;
};

useRequireJS?(
  returnOrThrowFunction(bwindow.setTimeout)(callbackFn)
):Function_call[call_key](
  returnOrThrowFunction(bdocument.addEventListener),
  bdocument,
  "DOMContentLoaded",
  callbackFn
);

    return bwindow;
  }
);